/*
 * File:   Request.hpp
 * Author: hrust
 *
 * Created on May 26, 2017, 4:21 PM
 */

#ifndef REQUEST_HPP
#define REQUEST_HPP

#include <string>
#include <vector>
#include <sstream>
#include <iostream>

enum Type
{
	Undefined,
	Create,
	Read,
	Update,
	Delete
};

class Request
{
public:
	Request(std::string Message);
	virtual ~Request();

	Type GetType();
	std::vector<std::string> GetBody();
	std::string GetResponse();
	void SetResponse(std::string Response);

private:
	std::vector<std::string> Split(std::string Message, char Delimiter);

	Type _Type;
	std::vector<std::string> _Body;
	std::string _Response;
};

#endif /* REQUEST_HPP */

