/*
 * File:   main.cpp
 * Author: synaps
 *
 * Created on May 6, 2017, 4:37 PM
 */

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <exception>

#include <vector>
#include <string>
#include <fstream>

#include <sys/socket.h>
#include <netinet/ip.h>
#include <unistd.h>

#include <experimental/filesystem>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/access.hpp>

#include <uuid/uuid.h>

#include "Track.hpp"
#include "Request.hpp"

void ProcessRequest(std::string pMessage);

namespace fs = std::experimental::filesystem;

std::vector<Track> _Tracks;
int _SocketDescriptor, _ClientDescriptor;

// more in socket(2)
struct Socket
{
	short Type;
	unsigned short Port;
	struct in_addr Address;
	char reserved[8];
};

void CreateSocket()
{
	// Open server socket
	_SocketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	if(_SocketDescriptor < 0)
	{
		std::cout << "Error opening socket.\n";
		std::terminate();
	}
	std::cout << "Socket opened.\n";

	// Bind address to socket
	// accepting incoming connections on port 4000
	struct Socket tServerSocket;
	tServerSocket.Type = AF_INET;
	tServerSocket.Port = htons(4000);
	tServerSocket.Address.s_addr = INADDR_ANY;

	if(bind(_SocketDescriptor, (struct sockaddr *) &tServerSocket, sizeof(tServerSocket)))
	{
		std::cout << "Error binding address.\n";
		std::terminate();
	}
	std::cout << "Address binded.\n";
}

void FreeSocket()
{
	close(_SocketDescriptor);
}

void AcceptClient()
{
	// Listen for connections
	listen(_SocketDescriptor, 5);

	// accept client
	struct Socket tClientSocket;
	unsigned int tClientLength = sizeof(tClientSocket);
	_ClientDescriptor = accept(_SocketDescriptor, (struct sockaddr *) &tClientSocket, &tClientLength);
	if(_ClientDescriptor < 0)
	{
		std::cout << "Error accepting a client.\n";
		std::terminate();
	}
	std::cout << "Client accepted.\n";
}

void Receive()
{
	// read from socket
	char tBuffer[256];
	memset(tBuffer, 0, sizeof(tBuffer));
	if(recv(_ClientDescriptor, tBuffer, sizeof(tBuffer) - 1, 0) > 0)
	{
		std::cout << tBuffer << '\n';
		ProcessRequest(tBuffer);
	}
}

void Send(std::string pMessage)
{
	write(_ClientDescriptor, pMessage.data(), pMessage.size());
}

void LoadData()
{	//FIXME interrupt handling
	std::ifstream tInputStream("data.bin", std::ios::binary);
	cereal::BinaryInputArchive tBinaryArchive(tInputStream);

	tBinaryArchive(_Tracks);
}

void SaveData()
{
	std::ofstream tOutputStream("data.bin", std::ios::binary);
	cereal::BinaryOutputArchive tBinaryArchive(tOutputStream);

	tBinaryArchive(_Tracks);

	tOutputStream.flush();
}

int Scan()
{
	// If path points to the directory
	fs::path tMediaDirectory = "/home/hrust/Music";
	if(!fs::is_directory(tMediaDirectory))
	{
		std::cout << "Not a directory.\n";
		std::terminate();
	}

	// Breadth-first search (BFS)
	std::vector<fs::directory_entry> tDirectories, tFiles;
	tDirectories.push_back(fs::directory_entry(tMediaDirectory));
	std::string tFileExtensions[] = {".mp3", ".aac", ".flac", ".ogg"};
	while(!tDirectories.empty())
	{
		fs::directory_entry tCurrent = tDirectories.back();
		tDirectories.pop_back();

		if(fs::is_regular_file(tCurrent))
		{
			tFiles.push_back(tCurrent);
			continue;
		}

		for(fs::directory_entry iEntry : fs::directory_iterator(tCurrent))
		{
			if(fs::is_directory(iEntry))
				tDirectories.push_back(iEntry);
			else if(fs::is_regular_file(iEntry))
			{
				std::string tExtension = iEntry.path().extension();
				for(int i = 0; i < sizeof(tFileExtensions)/sizeof(std::string); i++)
					if(tFileExtensions[i].compare(tExtension) == 0)
					{
						tFiles.push_back(iEntry);
						break;
					}
			}
		}
	}

	std::vector<Track> tTrackList;
	for(fs::directory_entry iEntry : tFiles)
	{
		//FIXME identify identical tracks
		fs::path tPath = iEntry.path();
		std::string tFilename = tPath.stem();
		unsigned long tSeperatorPosition = tFilename.find("-");
		std::string tArtist = tFilename.substr(0, tSeperatorPosition), tTitle = tFilename.substr(tSeperatorPosition + 1);
		uuid_t tUUID;
		uuid_generate(tUUID);
		Track tTrack(tUUID, tTitle, tArtist);
		tTrack.SetResource(tPath);
		tTrackList.push_back(tTrack);
	}

	_Tracks = tTrackList;		//FIXME update rather than replace
}

void ProcessRequest(std::string pMessage)
{
	Request tRequest(pMessage);
	std::vector<std::string> tBody = tRequest.GetBody();
	switch (tRequest.GetType())
	{
		case Create:
			break;
		case Read:
			// if UUID section is 0 then user asks for all the tracks
			if(tBody[0] == "0")
			{
				std::string tResponse;
				for(Track i : _Tracks)
				{
					tResponse.append(i.ToString() + '\n');
				}
				tRequest.SetResponse(tResponse);
			}
			break;
		case Update:
			break;
		case Delete:
			break;
		default:
			break;
	}
	std::cout << "Sending response\n";
	Send(tRequest.GetResponse());		//FIXME no need to get it from the object
}

int main(int argc, char** argv)
{
	// Load previously saved list of music
	//LoadData();
	// Scan media directory for new tracks
	Scan();
	// Create socket to be able to accept connections
	CreateSocket();
	// Accept client
	AcceptClient();
	Send("WOW");
	Receive();

	// Save data before exit
	SaveData();
	// Free socket
	FreeSocket();

	return 0;
}