/*
 * File:   Track.hpp
 * Author: hrust
 *
 * Created on May 7, 2017, 4:12 PM
 */

#ifndef TRACK_HPP
#define TRACK_HPP

#include <string>
#include <array>

#include <uuid/uuid.h>

#include <cereal/archives/binary.hpp>

class Track
{
public:
	Track();
	Track(uuid_t UUID, std::string Title, std::string Artist);
	virtual ~Track();

	void SetResource(std::string Path);
	std::string ToString();

	bool operator==(const Track& pRTrack);

	template <class Archive>
	void serialize(Archive &pArchive)
	{
		pArchive(_UUID, _Title, _Artist, _ResourcePath);
	}

private:
	std::string _UUID, _Title, _Artist, _ResourcePath;
};

#endif /* TRACK_HPP */

