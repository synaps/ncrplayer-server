/*
 * File:   Track.cpp
 * Author: hrust
 *
 * Created on May 7, 2017, 4:12 PM
 */

#include "Track.hpp"

Track::Track(){}
Track::Track(uuid_t pUUID, std::string pTitle, std::string pArtist)
{
	char tUUID[36];
	uuid_unparse_lower(pUUID, tUUID);
	_UUID = tUUID;
	_Title = pTitle;
	_Artist = pArtist;
}

Track::~Track()
{
}

void Track::SetResource(std::string pPath)
{
	_ResourcePath = pPath;
}

std::string Track::ToString()
{
	return _UUID + ";" + _Artist + ";" + _Title;
}

bool Track::operator==(const Track& pRTrack)
{
	return this->_UUID == pRTrack._UUID;
}
