/*
 * File:   Request.cpp
 * Author: hrust
 *
 * Created on May 26, 2017, 4:21 PM
 */

#include "Request.hpp"

Request::Request(std::string pMessage)
{
	/* First goes the type
	 * it can be GET
	 */
	std::string tMessage(pMessage);
	std::vector<std::string> tParts = Split(pMessage, ';');
	if (tParts[0] == "create")		// type of the request
		_Type = Create;
	else if (tParts[0] == "get")
		_Type = Read;
	else if (tParts[0] == "update")
		_Type = Update;
	else if (tParts[0] == "delete")
		_Type = Delete;
	else
		_Type = Undefined;
	tParts.erase(tParts.begin());		// remove type
	_Body = tParts;		// save the contents of the request
}


Request::~Request()
{
}

Type Request::GetType()
{
	return _Type;
}

std::vector<std::string> Request::GetBody()
{
	return _Body;
}

std::string Request::GetResponse()
{
	return _Response;
}
void Request::SetResponse(std::string pResponse)
{
	_Response = pResponse;
}

std::vector<std::string> Request::Split(std::string pMessage, char pDelimiter)
{
	std::vector<std::string> rParts;
	std::stringstream tStringStream(pMessage);
	std::string tPart;
	while(std::getline(tStringStream, tPart, pDelimiter))
	{
		rParts.push_back(tPart);
	}
	return rParts;
}